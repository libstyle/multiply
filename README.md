# multiply
 - console application for multiplying two signed numbers with length defined in macro: MAX_NUM_DIGITS
 - build: make
 - usage: ./multiply arg1 arg2 (e.g. ./multiply 1054877777 8944445222)