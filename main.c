/*
* multiply two integers application
* created: by Libor Klocan
*/

#include "multiply.h"

/*
* main
*/
int main(int argc, char **argv)
{
    char *mres = 0;
    char *res = 0;
    char *num1 = NULL;
    char *num2 = NULL;
    int ret, mrsz, rsz, n1sz, n2sz;

    ret = multiply_input_hdl(argc, &argv, &num1, &num2);
    if (ret < 0)
        return -1;

    multiply_get_size(num1, num2, &n1sz, &n2sz, &mrsz, &rsz);

    ret = multiply_nstr2intarr(&num1, &num2, n1sz, n2sz);
    if (ret < 0)
    {
        multiply_clean_up(num1, num2, NULL);
        return -1;
    }

    multiply_performance(num1, num2, mres, &res, mrsz, rsz, n1sz, n2sz);

    multiply_print_result(res, rsz);

    multiply_clean_up(num1, num2, res);

    return 0;
}