#include "multiply.h"

static int neg_flag = 0;

/*
* temporary result func - multiply by one digit from down number
*/
static int onelinem(char **mres, char *src, char num, int n1sz)
{
    int i, t, d, u;

    (*mres) = (char *)calloc((n1sz + 1), sizeof(char));

    for (i = 0; i < n1sz; i++)
    {
        t = (src[n1sz - 1 - i]) * num;
        if (t < DEC)
            (*mres)[n1sz - i] += t;
        else
        {
            d = t / DEC;
            u = t % DEC;

            (*mres)[n1sz - i] += u;
            (*mres)[n1sz - i - 1] += d;
        }
    }

    return 0;
}

/*
* subtraction of temp result to global result
*/
static int onelinea(char **res, char *mres, int shift, int rsz, int mrsz)
{
    int i, t, d, u, k = 0, tres;

    if ((*res) == NULL)
        (*res) = (char *)calloc(rsz, sizeof(char));

    for (i = 0; i < mrsz; i++)
    {
        t = ((*res)[rsz - 1 - shift - i]) + (mres[mrsz - 1 - i]);

        if (t < DEC)
        {
            (*res)[rsz - 1 - shift - i] = t;
        }
        else
        {
            d = 1;
            u = t - DEC;

            (*res)[rsz - 1 - shift - i] = u;

            (*res)[rsz - 1 - shift - i - 1] += d;

            tres = (*res)[rsz - 1 - shift - i - 1];

            k = 0;
            while (tres == DEC)
            {
                k++;
                (*res)[rsz - 1 - shift - i - 1 - k] += 1;
                tres = (*res)[rsz - 1 - shift - i - 1 - k];
                (*res)[rsz - 1 - shift - i - 1 - k + 1] = 0;
            }
        }
    }
    return 0;
}

/*
* string representation of numbers converted to int digit
*/
int multiply_nstr2intarr(char **n1, char **n2, int n1len, int n2len)
{
    int i;

    for (i = 0; i < n1len; i++)
        (*n1)[i] -= ZERO_ASCII;

    for (i = 0; i < n2len; i++)
        (*n2)[i] -= ZERO_ASCII;

    return 0;
}

/*
* handling of input arguments
*/
int multiply_input_hdl(int ac, char ***av, char **n1, char **n2)
{
    int a1len, a2len, i, k, tlen;

    if (ac != NUM_ARGS)
    {
        printf("Error: two input arguments required : multiply 'NUM1' 'NUM2'\n");
        return -1;
    }

    a1len = strnlen((*av)[1], MAX_NUM_DIGITS + 1);
    a2len = strnlen((*av)[2], MAX_NUM_DIGITS + 1);

    if (a1len > MAX_NUM_DIGITS || a2len > MAX_NUM_DIGITS)
    {
        printf("Error: input arguments too big (arg1 or arg2 > %d)\n", MAX_NUM_DIGITS);
        return -1;
    }

    if ((*av)[1][0] == MINUS && (*av)[2][0] == MINUS)
    {
        (*av)[1][0] = ZERO_ASCII;
        (*av)[2][0] = ZERO_ASCII;
    }

    if ((*av)[1][0] == MINUS || (*av)[2][0] == MINUS)
    {
        neg_flag = 1;

        if ((*av)[1][0] == MINUS)
            (*av)[1][0] = ZERO_ASCII;

        if ((*av)[2][0] == MINUS)
            (*av)[2][0] = ZERO_ASCII;
    }

    tlen = a1len;
    for (k = 0; k < (NUM_ARGS - 1); k++)
    {
        for (i = 0; i < tlen; i++)
        {
            if ((*av)[k + 1][i] < ZERO_ASCII || (*av)[k + 1][i] > NINE_ASCII)
            {
                printf("Error: only digits 0 - 9 are allowed\n");
                return -1;
            }
        }
        tlen = a2len;
    }

    if (a1len > a2len)
    {
        (*n1) = (char *)calloc(a1len + 2, sizeof(char));
        strncpy((*n1), (*av)[1], a1len + 2);
        (*n2) = (char *)calloc(a2len + 1, sizeof(char));
        strncpy((*n2), (*av)[2], a2len + 1);
    }
    else
    {
        (*n2) = (char *)calloc(a1len + 1, sizeof(char));
        strncpy((*n2), (*av)[1], a1len + 1);
        (*n1) = (char *)calloc(a2len + 1, sizeof(char));
        strncpy((*n1), (*av)[2], a2len + 1);
    }

    return 0;
}

/*
* get required size from input numbers
*/
void multiply_get_size(char *n1, char *n2, int *n1sz, int *n2sz, int *mrsz, int *rsz)
{
    *mrsz = strlen(n1) + 1;
    *rsz = strlen(n1) * 2;

    *n1sz = strlen(n1);
    *n2sz = strlen(n2);
}

/*
* clean up
*/
void multiply_clean_up(char *e1, char *e2, char *e3)
{
    if (e1 != NULL)
        free(e1);
    if (e2 != NULL)
        free(e2);
    if (e3 != NULL)
        free(e3);
}

/*
* performance of main multiply func
*/
int multiply_performance(char *n1, char *n2, char *mr, char **r, int mrsz, int rsz,
    int n1sz, int n2sz)
{
    int k;

    for (k = 0; k < n2sz; k++)
    {
        onelinem(&mr, n1, n2[n2sz - 1 - k], n1sz);
        onelinea(&(*r), mr, k, rsz, mrsz);
        free(mr);
    }

    return 0;
}

/*
* print global result
*/
void multiply_print_result(char *res, int rsz)
{
    int i, numflag = 0;

    printf("\n\n");
    printf("result : ");

    for (i = 0; i < rsz; i++)
    {
        if (res[i] == 0 && !numflag)
            continue;
        else
        {
            if (!numflag && neg_flag)
                printf("-");

            numflag = 1;
            printf("%d", res[i]);
        }
    }
    printf("\n\n");
}