CC=gcc

TARGET=multiply
OBJECTS=main.o multiply.o

$(TARGET): $(OBJECTS)
	   $(CC) $(OBJECTS) -o $(TARGET)

multiply.o: multiply.c
	$(CC) -c -Wall multiply.c

main.o: main.c
	$(CC) -c -Wall main.c

.PHONY: clean

clean:
	rm *.o multiply
