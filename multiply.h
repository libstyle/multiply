#ifndef MULTIP_H
#define MULTIP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

/*
* MACROS
*/
#define NUM_ARGS 3
#define DEC 10
#define NINE_ASCII 0x39
#define ZERO_ASCII 0x30
#define MINUS '-'
#define MAX_NUM_DIGITS 200

/*
* FUNC DECS
*/
extern int multiply_input_hdl(int ac, char ***av, char **n1, char **n2);
extern int multiply_nstr2intarr(char **n1, char **n2, int n1len, int n2len);

extern void multiply_get_size(char *n1, char *n2, int *n1sz, int *n2sz,
    int *mrsz, int *rsz);

extern int multiply_performance(char *n1, char *n2, char *mr, char **r,
    int mrsz, int rsz, int n1sz, int n2sz);

extern void multiply_print_result(char *res, int rsz);

extern void multiply_clean_up(char *e1, char *e2, char *e3);

#endif